package com.xtrf.weather;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.xtrf.weather.WeatherConstant.*;

@Slf4j
@Component
public class WeatherMonitor implements Runnable {

    @Value("${weather.city.id}")
    private String cityId;

    private final WeatherClient client;
    private final ScheduledExecutorService scheduler;

    @Autowired
    public WeatherMonitor(WeatherClient client) {
        this.client = client;
        scheduler = Executors.newScheduledThreadPool(POOL_SIZE);
    }

    @PostConstruct
    public void init() {
        log.info("Initializing weather monitor");
        Thread thread = new Thread(this);
        scheduler.scheduleAtFixedRate(thread, DELAY, PERIOD, TimeUnit.DAYS);
    }

    @Override
    public void run() {
        log.info("Weather monitor is scanning...");
        Weather weather = client.getWeather(cityId);
        client.postWeather(weather);
    }
}
