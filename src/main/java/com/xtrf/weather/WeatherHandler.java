package com.xtrf.weather;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.xtrf.weather.WeatherConstant.*;

@Component
public class WeatherHandler {

    @Value("${date.format}")
    private String dateFormat;

    private ObjectMapper mapper;

    @Autowired
    public WeatherHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public Weather toWeather(String response) {
        Weather weather = null;
        try {
            JsonNode jsonResponse = mapper.readTree(response);
            String currentWeather = getWeather(jsonResponse);
            double temperature = getTemperature(jsonResponse);
            long timestamp = getTimestamp(jsonResponse);
            String sunrise = getSunrise(jsonResponse);
            String sunset = getSunset(jsonResponse);
            weather = new Weather(currentWeather, temperature, timestamp, sunrise, sunset);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return weather;
    }

    private String getWeather(JsonNode jsonResponse) {
        return jsonResponse.get(WEATHER).get(0).get(MAIN).asText();
    }

    private double getTemperature(JsonNode jsonResponse) {
        double temp = jsonResponse.get(MAIN).get(TEMP).asDouble();
        return toCelsius(temp);
    }

    private double toCelsius(double temp) {
        return BigDecimal.valueOf(temp - TEMP_FACTOR)
                .setScale(DOUBLE_SCALE, RoundingMode.HALF_UP)
                .doubleValue();
    }

    private long getTimestamp(JsonNode jsonResponse) {
        return jsonResponse.get(DT).asLong();
    }

    private String getSunrise(JsonNode jsonResponse) {
        long unix = jsonResponse.get(SYS).get(SUNRISE).asLong();
        return toDate(unix);
    }

    private String getSunset(JsonNode jsonResponse) {
        long unix = jsonResponse.get(SYS).get(SUNSET).asLong();
        return toDate(unix);
    }

    private String toDate(long unix) {
        Date date = new Date(unix * UNIX_FACTOR);
        return formatDate(date);
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat(dateFormat).format(date);
    }
}
