package com.xtrf.weather;

public class WeatherConstant {
    public static final String MAIN = "main";
    public static final String WEATHER = "weather";
    public static final String TEMP = "temp";
    public static final String DT = "dt";
    public static final String SYS = "sys";
    public static final String SUNRISE = "sunrise";
    public static final String SUNSET = "sunset";

    public static final int DOUBLE_SCALE = 2;
    public static final double TEMP_FACTOR = 272.15D;
    public static final int UNIX_FACTOR = 1000;

    public static final int POOL_SIZE = 1;
    public static final int DELAY = 0;
    public static final int PERIOD = 1;

    private WeatherConstant() {
    }
}
