package com.xtrf.weather;

import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class WeatherClientImpl implements WeatherClient {

    @Value("${weather.url.source}")
    private String sourceUrl;
    @Value("${weather.url.target}")
    private String targetUrl;
    @Value("${weather.api.key}")
    private String apiKey;

    private WeatherHandler handler;

    @Autowired
    public WeatherClientImpl(WeatherHandler handler) {
        this.handler = handler;
    }

    @Override
    public Weather getWeather(String cityId) {
        String response = Unirest
                .get(sourceUrl)
                .queryString("id", cityId)
                .queryString("appid", apiKey)
                .asJson()
                .ifSuccess(r -> log.info("Received raw weather data: {}", r.getBody()))
                .ifFailure(r -> log.error("Error during receiving raw weather data: {}", r.getBody()))
                .getBody()
                .toString();

        return handler.toWeather(response);
    }

    @Override
    public void postWeather(Weather weather) {
        Unirest.post(targetUrl)
                .header("Content-Type", "application/json")
                .body(weather)
                .asString()
                .ifSuccess(r -> log.info("Weather data successfully sent: {}", weather))
                .ifFailure(r -> log.error("Error during sending weather data: {}", r.getStatus()));
    }
}
