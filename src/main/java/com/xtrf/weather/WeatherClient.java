package com.xtrf.weather;

import org.springframework.stereotype.Component;

@Component
public interface WeatherClient {
    Weather getWeather(String cityId);

    void postWeather(Weather weather);
}
