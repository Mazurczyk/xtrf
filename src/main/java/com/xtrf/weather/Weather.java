package com.xtrf.weather;

import lombok.Data;

@Data
public class Weather {
    private String currentWeather;
    private double currentTemperature;
    private long timestamp;
    private String sunrise;
    private String sunset;

    public Weather(String currentWeather, double currentTemperature, long timestamp, String sunrise, String sunset) {
        this.currentWeather = currentWeather;
        this.currentTemperature = currentTemperature;
        this.timestamp = timestamp;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }
}
