FROM adoptopenjdk:11-jdk-hotspot as builder
WORKDIR application
COPY *.gradle gradlew ./
COPY gradle gradle
RUN chmod +x gradlew && ./gradlew build
COPY src src
RUN ./gradlew bootJar
ARG JAR_FILE=build/libs/*.jar
RUN java -Djarmode=layertools -jar ${JAR_FILE} extract

FROM adoptopenjdk:11-jre-hotspot
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["java", "com.xtrf.weather.WeatherApplication"]